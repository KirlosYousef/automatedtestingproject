package hu.unideb.inf;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public class StepDefinitions {

    private static final int WAIT_TIME = 10;

    static WebDriver driver;

    private HomePage homePage;

    static {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("--no-sandbox");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(WAIT_TIME, TimeUnit.SECONDS);
    }

    @Given("The home page is opened")
    public void theHomePageIsOpened() { homePage = new HomePage(driver); }

    @And("The Sign In is clicked")
    public void theSignInIsClicked() { homePage.clickSignInLink(); }

    @Given("The Sign In Button is clicked")
    public void TheSignInButtonIsClicked() { homePage.clickSignInButton(); }

    @Given("The Create an account Button is clicked")
    public void TheCreateAnAccountButtonIsClicked() { homePage.clickCreateAnAccountButton(); }

    @Given("The {string} is filled in with {string}")
    public void theFieldIsFilledInWithValue(String field, String value) { homePage.fillOutField(field, value); }

    @Given("The Search Button is clicked")
    public void TheSearchButtonIsClicked() { homePage.clickSearchButton(); }

    @Given("The Result More Button is clicked")
    public void TheResultIsClicked() { homePage.clickMoreButton(); }

    @Given("The Add To Cart Button is clicked")
    public void TheAddToCartButtonIsClicked() { homePage.clickAddToCartButton(); }

    @Given("Check out Button is clicked")
    public void TheCheckOutButtonIsClicked() { homePage.clickCheckOutButton(); }

    @Given("Delete Button is clicked")
    public void TheDeleteButtonIsClicked() { homePage.clickDeleteButton(); }

    @Given("Wait 5 Seconds")
    public void Wait() {
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Then("The {string} appears in {string} alert")
    public void theAlertIsShown(String msg, String xPath) {
        Optional<String> errorMessage = homePage.getAlertMessage(xPath);
        if (errorMessage.isPresent()) {
            Assert.assertEquals(msg, errorMessage.get());
        } else {
            fail();
        }
    }
}
