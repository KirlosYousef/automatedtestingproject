package hu.unideb.inf;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Optional;

public class HomePage {

    private static final String PAGE_URL = "http://automationpractice.com/";

    private WebDriver driver;

    @FindBy(className = "login")
    private WebElement signInLink;

    @FindBy(id = "SubmitLogin")
    private WebElement SignInButton;

    @FindBy(id = "SubmitCreate")
    private WebElement CreateAnAccountButton;

    @FindBy(name = "submit_search")
    private WebElement SearchButton;

    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li[1]/div/div[2]/div[2]/a[2]/span")
    private WebElement ResultMoreButton;

    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li[1]/div/div[2]/div[2]/a[1]/span")
    private WebElement AddToCartButton;

    @FindBy(xpath = "//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a/span")
    private WebElement CheckOutButton;

    @FindBy(xpath = "//*[@id=\"5_19_0_0\"]/i")
    private WebElement DeleteButton;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        driver.get(PAGE_URL);
        PageFactory.initElements(driver, this);
    }

    public void clickSignInLink() {
        getSignInLink().click();
    }

    public void clickCreateAnAccountButton() {
        getCreateAnAccountButton().click();
    }

    public void clickSignInButton() {
        getSignInButton().click();
    }

    public void clickSearchButton() {
        getSearchButton().click();
    }

    public void clickMoreButton() {
        getMoreButton().click();
    }

    public void clickAddToCartButton() {
        getAddToCartButton().click();
    }

    public void clickCheckOutButton() {
        getCheckOutButton().click();
    }

    public void clickDeleteButton() {
        getDeleteButton().click();
    }

    public void fillOutField(String field, String value){ findField(field).sendKeys(value); }

    public WebElement findField(String field) {
        return driver.findElement((By.id(field)));
    }

    public Optional<String> getAlertMessage(String xPath){
        Optional<WebElement> error = getAlert(xPath);
        if (error.isPresent()) {
            WebElement errorElement = error.get();
            return Optional.of(errorElement.getText());
        } else {
            return Optional.empty();
        }
    }

    public WebElement getSignInLink() {
        return signInLink;
    }

    public WebElement getCreateAnAccountButton() {
        return CreateAnAccountButton;
    }

    public WebElement getSignInButton() {
        return SignInButton;
    }

    public WebElement getSearchButton() { return SearchButton; }

    public WebElement getMoreButton() {
        return ResultMoreButton;
    }

    public WebElement getAddToCartButton() {
        return AddToCartButton;
    }

    public WebElement getCheckOutButton() {
        return CheckOutButton;
    }

    public WebElement getDeleteButton() {
        return DeleteButton;
    }

    private Optional<WebElement> getAlert(String xPath) {
        List<WebElement> webElements = driver.findElements(By.xpath(xPath));
        if (webElements.size() > 0) {
            return Optional.of(webElements.get(0));
        } else {
            return Optional.empty();
        }
    }
}
