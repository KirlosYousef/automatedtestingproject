Feature: YourLogo SignUP Page
  Background:
    Given The home page is opened
    And The Sign In is clicked

  Scenario Outline: Create An Account
    Given The '<field>' is filled in with '<value>'
    And The Create an account Button is clicked
    Then The '<alert>' appears in '<alertXpath>' alert
    Examples:
      | field        | value             | alertXpath                            |  alert                        |
      | email_create | invalid.email.com | //*[@id="create_account_error"]/ol/li |  Invalid email address.       |
      | email_create | kk@gmail.com      | //*[@id="create_account_error"]/ol/li |  An account using this email address has already been registered. Please enter a valid password or request a new one.      |

  Scenario Outline: Sign In Using Invalid Data
    Given The '<field>' is filled in with '<value>'
    And The Sign In Button is clicked
    Then The '<alert>' appears in '<alertXpath>' alert
    Examples:
      | field        | value             | alertXpath                            |  alert                      |
      | email        | invalid.email.com | //*[@id="center_column"]/div[1]/ol/li |  Invalid email address.     |
      | email        | valid@email.com   | //*[@id="center_column"]/div[1]/ol/li |  Password is required.      |
      | email        |                   | //*[@id="center_column"]/div[1]/ol/li |  An email address required. |

  Scenario Outline: Search Empty Or No Matches Product
    Given The '<field>' is filled in with '<value>'
    And The Search Button is clicked
    Then The '<alert>' appears in '<alertXpath>' alert
    Examples:
      | field            | value  | alertXpath                 |  alert                                       |
      | search_query_top | test   | //*[@id="center_column"]/p |  No results were found for your search "test"|
      | search_query_top |        | //*[@id="center_column"]/p |  Please enter a search keyword               |

  Scenario Outline: Search Valid Product And Open Its Info
    Given The '<field>' is filled in with '<value>'
    And The Search Button is clicked
    And The Result More Button is clicked
    Then The '<alert>' appears in '<alertXpath>' alert
    Examples:
      | field            | value                  | alertXpath                                  |  alert                |
      | search_query_top | Printed Summer Dress   | //*[@id="center_column"]/div/div/div[3]/h1  |  Printed Summer Dress |

  Scenario Outline: Add Product To Cart And Delete It
    Given The '<field>' is filled in with '<value>'
    And The Search Button is clicked
    And The Add To Cart Button is clicked
    And Check out Button is clicked
    And Delete Button is clicked
    And Wait 5 Seconds
    Then The '<alert>' appears in '<alertXpath>' alert
    Examples:
      | field            | value                | alertXpath                 | alert                        |
      | search_query_top | Printed Summer Dress | //*[@id="center_column"]/p | Your shopping cart is empty. |